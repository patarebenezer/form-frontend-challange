import Header from "../../components/Header";
import PlayerMusic from "../../components/PlayerMusic";
import Form from "../../components/Form";
import Sidebar from "../../components/Sidebar";

const Home = () => {
 return (
  <div className="mb-10">
   <Header />
   <div className="flex-row lg:flex justify-around mt-10 px-14 lg:px-24 gap-16">
    <div className="w-full lg:w-3/4">
     <PlayerMusic />
     <Form />
    </div>
    <Sidebar />
   </div>
  </div>
 );
};

export default Home;
