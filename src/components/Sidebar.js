const Sidebar = () => {
 const totalQuestions = Array.from({ length: 24 }, (_, i) => i + 1);

 return (
  <div className="w-full lg:w-2/5 h-[44rem] bg-gray-100 font-bold text-gray-500 px-10 pt-12 mt-10 lg:mt-0">
   <h1>Questions Status</h1>
   <p className="text-gray-400 antialiased">Lorem ipsum dolor sit amet</p>
   <div className="grid grid-cols-7 my-7 justify-center items-center gap-y-3 gap-x-0">
    {totalQuestions.map((num) => (
     <p
      className={`${
       num === 10 || num === 23 || num === 24 ? "bg-red-100" : "bg-emerald-100"
      } w-7 h-7 text-sm rounded-full flex justify-center items-center border`}
      key={num}
     >
      {num}
     </p>
    ))}
   </div>
   <p className="mb-2 mt-8">Ket</p>
   <div className="flex gap-2 items-center">
    <span
     className={`bg-emerald-100 w-7 h-7 text-sm rounded-full flex justify-center items-center`}
    ></span>
    <p className="text-gray-400">Answered</p>
   </div>
   <div className="flex gap-2 items-center mt-2">
    <span
     className={`bg-red-100 w-7 h-7 text-sm rounded-full flex justify-center items-center`}
    ></span>
    <p className="text-gray-400">Unanswered</p>
   </div>
  </div>
 );
};

export default Sidebar;
