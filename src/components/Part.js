const Part = ({ no_part, no_questions }) => {
 return (
  <div className="font-bold antialiased w-full bg-blue-100 py-2 mt-8">
   <div className="flex items-center ml-4">
    <span className="text-lg">Part {no_part} </span>
    <span className="text-gray-500 text-sm ml-2">{`(Questions ${no_questions})`}</span>
   </div>
  </div>
 );
};

export default Part;
