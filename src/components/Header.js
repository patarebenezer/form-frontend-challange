import { ReactComponent as IconX } from "../assets/icons/icon-x.svg";

const Header = () => {
 return (
  <>
   <div className="bg-gray-200 py-4 flex justify-between px-14 lg:px-24">
    <h1 className="text-xl font-semibold">Practice 1 - Listening Test</h1>
    <div className="flex items-center">
     <button className="bg-blue-100 rounded text-gray-400 font-bold antialiased border-2 border-blue-400 text-xs py-0.5 px-2">
      Instruction
     </button>
     <button className=" bg-red-600 hover:bg-red-500 ml-4 text-white text-xs font-bold antialiased rounded-full lg:w-full lg:h-full w-8 h-8 lg:py-1 px-3">
      <div className="gap-1 items-center hidden lg:flex">
       <IconX />
       <span>Exit</span>
      </div>
      <span className="block lg:hidden">X</span>
     </button>
    </div>
   </div>
  </>
 );
};

export default Header;
