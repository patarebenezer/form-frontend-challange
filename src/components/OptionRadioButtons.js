import { useState } from "react";
import RadioButton from "./RadioButton";

const OptionRadioButtons = () => {
 const [selectedOption, setSelectedOption] = useState("");

 const handleOptionChange = (event) => {
  setSelectedOption(event.target.value);
 };

 return (
  <div className="mb-7 mt-2">
   <RadioButton
    value="germany"
    label="Germany"
    name="options"
    checked={selectedOption === "germany"}
    onChange={handleOptionChange}
   />
   <RadioButton
    value="russia"
    label="Russia"
    name="options"
    checked={selectedOption === "russia"}
    onChange={handleOptionChange}
   />
   <RadioButton
    value="australia"
    label="Australia"
    name="options"
    checked={selectedOption === "australia"}
    onChange={handleOptionChange}
   />
   <RadioButton
    value="indonesia"
    label="Indonesia"
    name="options"
    checked={selectedOption === "indonesia"}
    onChange={handleOptionChange}
   />
  </div>
 );
};

export default OptionRadioButtons;
