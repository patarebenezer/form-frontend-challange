import BtnListen from "./BtnListen";

const QuestionTitle = ({ no }) => {
 return (
  <div className="flex items-center gap-4 my-4">
   <h1 className="text-sky-500 font-bold antialiased text-lg">
    Questions {no}
   </h1>
   <BtnListen />
  </div>
 );
};

export default QuestionTitle;
