import Part from "./Part";
import Question from "./Question";
import QuestionTitle from "./QuestionTitle";
import getEssayQuestion from "./getEssayQuestion";

const Form = () => {
 const dots = "...............";
 const btn_cls = "w-full text-white font-bold antialiased bg-indigo-600 py-3 rounded-md hover:bg-indigo-700";
 const img_cls = "w-full h-full lg:h-[25rem] rounded p-10 object-cover";
 const url_img = "https://clicxy.com/wp-content/uploads/2016/04/dummy-post-horisontal.jpg";

 return (
  <div>
   <div className="part1">
    <Part no_part={"1"} no_questions={"1-10"} />
    <QuestionTitle no={"1-6"} />
    <p className="font-bold text-gray-800 antialiased italic my-3">
     Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    </p>
    <Question no={1} question={"What is the women from ?"} type={"radiobtn"} />

    <Question
     no={2}
     question={
      "The woman says that you can travel from Croatia to Germany in two hours by"
     }
     type={"radiobtn"}
    />

    <QuestionTitle no={"7-10"} />
    <p className="mt-4 mb-5 font-bold text-gray-400 antialiased">
     Complete the form below, using NO MORE THAN TREE WORDS AND/OR NUMBER for
     each answer
    </p>
    <div className="bg-gray-100 text-gray-400 p-8 font-bold antialiased">
     <h1 className="text-xl text-center">Travel Safe</h1>
     <p className="my-6">Department: Motor insurance</p>
     <p>Client details:</p>
     <p className="flex gap-2 my-1">
      Name: Elisabeth <Question no={7} question={dots} />
     </p>
     <p>Date of birth: 8. 10. 1975</p>
     <p className="flex gap-2 my-1">
      Address:
      <Question no={8} question={dots + "(street) Callington (town)"} />
     </p>
     <p className="flex gap-2 my-1">
      Policy number:
      <Question no={10} question={dots} />
     </p>
    </div>
   </div>

   <div className="part2">
    <Part no_part={"2"} no_questions={"11-24"} />
    <div className="my-8">
     <QuestionTitle no={"11-13"} />
     <div className="font-bold antialiased italic text-gray-600">
      <p>Label the diagram/plan below</p>
      <p>Write the correct letter, A - G next to questions 11-13</p>
     </div>
     <img src={url_img} alt="dummy-img" className={img_cls} />
     {[11, 12, 13].map((no) => (
      <div key={no} className="my-2">
       <Question no={no} type="essay" question={getEssayQuestion(no)} />
      </div>
     ))}
    </div>

    <div className="my-8">
     <QuestionTitle no="14-20" />
     <div className="font-bold antialiased italic text-gray-600">
      <p className="font-bold italic my-4">
       Answer the following questions NO MORE THAN TREE WORDS AND/OR NUMBER
      </p>
     </div>
     {[14, 15, 16].map((no) => (
      <div key={no} className="my-2">
       <Question no={no} type="essay" question={getEssayQuestion(no)} />
      </div>
     ))}
    </div>
   </div>

   <button type="submit" className={btn_cls}>
    Submit
   </button>
  </div>
 );
};

export default Form;
