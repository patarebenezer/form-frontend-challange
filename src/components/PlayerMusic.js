import { useEffect, useState } from "react";
import ReactAudioPlayer from "react-audio-player";
import music from "../assets/mp3/komang.mp3";

const PlayerMusic = () => {
 const [time, setTime] = useState(10);

 useEffect(() => {
  const timer = setInterval(() => {
    setTime((prevTime) => (prevTime > 0 ? prevTime - 1 : 0));
  }, 1000);

  return () => {
    clearInterval(timer);
  };
}, []);

const formatTime = (seconds) => {
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds % 60;
  return `${minutes}:${remainingSeconds < 10 ? "0" : ""}${remainingSeconds}`;
};

 return (
  <>
   <div className="flex w-full items-center">
    <h1 className="font-bold antialiased text-xl">Listening Tip</h1>
    <span className={`${time === 0 ? 'bg-red-500' : 'bg-sky-600'} font-bold antialiased ml-4 rounded-xl text-white px-8 text-sm py-1`}>
     Time Left : {formatTime(time)}
    </span>
   </div>
   <p className="my-4 text-gray-500 font-semibold antialiased">
    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Omnis autem neque
    culpa accusamus molestiae, veritatis dolor! Vitae laboriosam iure tempore
    illum quasi illo provident voluptates facere repellendus accusantium, dolor
    doloremque.
   </p>
   <ReactAudioPlayer src={music} controls className="w-full" />
  </>
 );
};

export default PlayerMusic;
