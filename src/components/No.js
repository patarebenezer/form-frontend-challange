const No = ({ no }) => {
 return (
  <p className="bg-gray-200 w-[1.5rem] flex justify-center items-center">
   {no}
  </p>
 );
};

export default No;
