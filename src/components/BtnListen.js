import headset from "../assets/icons/headsets.png";

const BtnListen = () => {
 return (
  <div className="flex items-center gap-1 border-2 border-sky-500 cursor-pointer text-gray-400 text-sm font-bold antialiased px-2 bg-blue-100">
   <img src={headset} alt="" className="w-4" />
   <span>Click here for listen </span>
  </div>
 );
};

export default BtnListen;
