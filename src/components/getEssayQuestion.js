const getEssayQuestion = (no) => {
 switch (no) {
  case 11:
   return "traffic lights";
  case 12:
   return "petrol station";
  case 13:
   return "blue van";
  case 14:
   return "What end of the market are the properties?";
  case 15:
   return "What does the speaker compare buying houses with?";
  case 16:
   return "How can you ask the speaker a question?";
  default:
   return "";
 }
};

export default getEssayQuestion;
