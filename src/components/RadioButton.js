const RadioButton = ({ value, label, name, checked, onChange }) => {
 return (
  <div className="flex items-center mb-2">
   <input
    type="radio"
    id={value}
    name={name}
    value={value}
    checked={checked}
    onChange={onChange}
    className="mr-2"
   />
   <label htmlFor={value} className="font-bold text-gray-400 antialiased">
    {label}
   </label>
  </div>
 );
};

export default RadioButton;
