import No from "./No";
import OptionRadioButtons from "./OptionRadioButtons";

const Question = ({ no, question, type }) => {
 return (
  <div className="flex gap-4 text-gray-400 font-bold antialiased">
   <div>
    <No no={no} />
   </div>
   <div>
    <p>{question}</p>
    {type === "radiobtn" && <OptionRadioButtons />}
    {type === "essay" && (
     <p className="text-gray-700 my-4 font-bold antialiased italic">
      answer .........................
     </p>
    )}
   </div>
  </div>
 );
};

export default Question;
